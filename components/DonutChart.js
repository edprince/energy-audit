import { PieChart } from 'react-minimal-pie-chart';

export default function DonutChart(props) {
	return (
		<PieChart
			animationEasing='ease-out'
			animate={true}
			lineWidth={15}
			rounded
			label={({ dataEntry }) => Math.round(dataEntry.value)}
			labelStyle={{
				fontSize: "2rem",
				fill: props.color
			}}
			labelPosition={0}
			totalValue={100}
			data={[
				{ title: 'Heating', value: props.value, color: props.color },
			]}
		/>
	)
}