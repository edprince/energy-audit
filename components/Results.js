import { Page, Text, View, Document, StyleSheet } from '@react-pdf/renderer';
import ReactPDF from '@react-pdf/renderer';
import { PDFViewer } from '@react-pdf/renderer';
import { Transition } from '@headlessui/react';
import getPercentage from '../lib/utils';
import DonutChart from './DonutChart';
import advice from '../lib/advice';
import Tip from './Tip';


// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  }
});

// Create Document Component
const MyDocument = () => (
  <Document>
    <Page size="A4" style={styles.page}>
      <View style={styles.section}>
        <Text style={{textAlign: "center", fontWeight: "bold"}}>Energy Results</Text>
      </View>
    </Page>
  </Document>
);

export default function Results(props) {
	let results = props.results;
	let lightingPercentage = 0;
	let heatingPercentage = 0;
	let appliancesPercentage = 0;

	if (props.show) {
		let lighting = results.q3.response.val || 0;
		let lightingMax = results.q3.response.total || 0;
		lightingPercentage = getPercentage(lighting, lightingMax);

		let heating = results.q5.response.val || 0;
		let heatingMax = results.q5.response.total || 0;
		heatingPercentage = getPercentage(heating, heatingMax)

		let appliances = results.q6.response.val || 0;
		let appliancesMax = results.q6.response.total || 0;
		appliancesPercentage = getPercentage(appliances, appliancesMax);
	}
	/*
			<PDFViewer className='shadow-lg w-full lg:w-2/3 m-auto h-96 rounded'>
				<MyDocument />
			</PDFViewer>
			*/

	
	return (
		<Transition show={props.show}>
			<span className='text-3xl font-bold text-center'>Results</span>
			<div className='grid grid-cols-3 px-16 md:px-0 md:grid-cols-3 gap-8 pt-8 mb-16'>
				<div className='text-center'>
					<h1 className='text-2xl'>Heating</h1>
					<DonutChart value={heatingPercentage} color='#F87171' />
				</div>
				<div className='text-center'>
					<h1 className='text-2xl'>Lighting</h1>
					<DonutChart value={lightingPercentage} color='#FBBF24' />
				</div>
				<div className='text-center'>
					<h1 className='text-2xl'>Appliances</h1>
					<DonutChart value={appliancesPercentage} color='#6366F1'/>
				</div>
			</div>
			<h1 className='text-2xl font-bold mb-8'>Advice</h1>
			{advice.map((tip) => ( <Tip title={tip.title} description={tip.description}/>))}
			<button onClick={() => window.print()} className='bg-green-500 hover:bg-green-400 text-white font-bold rounded py-3 px-6 mt-24'>Save Results</button>
		</Transition>
	)
}