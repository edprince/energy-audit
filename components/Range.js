import { useState } from 'react'
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

export default function Range(props) {
	const [state, setState] = useState(0);
	let total = Object.keys(props.options).length;
	let max = total - 1;
	return (
		<div className='px-4'>
			<Slider 
				step={1}
				min={0} 
				max={max} 
				ariaLabelForHandle='Slider Aria'
				marks={props.options} 
				trackStyle={{
					background: '#10b981'
				}}
				onChange={(e) => props.dispatch({
					...props.res, 
					response: {val: e + 1, total: total}
					}
				)}
			/>
		</div>
	)
}