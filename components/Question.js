import Popover from './Popover'

export default function Question(props) {
	return (
        <div className="text-left justify-around max-w-4xl mb-12 w-full">
			<h2 className='text-xl font-semibold mb-2'>{props.title}</h2>
			<p className='text-gray-500 mt-2 mb-4'>{props.description} <span className='text-gray-500 underline'>Read More</span></p>
			{ props.children }
        </div>
	)
}