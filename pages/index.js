import Head from 'next/head'
import { Transition } from '@headlessui/react'
import { useEffect, useState, useReducer } from 'react'
import sections from '../lib/data';

import Progress from '../components/Progress'
import Section from '../sections/Section'
import Results from '../components/Results'

function surveryReducer(state, action) {
	switch (action.type) {
		case "set-response": {
			return {
				...state,
				[action.questionId]: {
					response: action.response
				}
			}
		}
	}
}

export default function Home() {
	let initialState = {};
	sections.forEach(eachSection => {
		eachSection.forEach(question => {
			initialState[question.questionId] = {response: {val: 0, max: 1}};
		})
	});
	const [section, setSection] = useState(1);
	const [state, dispatch] = useReducer(surveryReducer, initialState)
	const [showResults, setShowResults] = useState(false);
	const TOTAL_SECTIONS = 2;

	useEffect(() => {
		console.log(state);
	})


	function submit() {
		setShowResults(true);
	}

	function nextSection() {
		if (section < TOTAL_SECTIONS) {
			setSection(section + 1)
		}

		window.scrollTo(0, 0);
	}
	
	function lastSection() {
		if (section > 1) {
			setShowResults(false);
			setSection(section - 1)
		}
		window.scrollTo(0, 0);
	}

	return (
		<div className="flex flex-col items-center justify-center min-h-screen py-2 pt-8">
		<Head>
			<title>Energy Audit</title>
			<meta name="description" content="Energy auditing tool"></meta>
			<link rel="icon" href="/favicon.ico" />
			<link rel="manifest" href="/manifest.json" />
			<link href='/favicon-16x16.png' rel='icon' type='image/png' sizes='16x16' />
			<link href='/favicon-32x32.png' rel='icon' type='image/png' sizes='32x32' />
			<link rel="apple-touch-icon" href="/apple-icon.png"></link>
			<meta name="theme-color" content="#317EFB"/>
		</Head>

		<main className="flex flex-col items-center justify-center max-w-screen-sm w-full md:w-2/3 lg:w-1/2 mx-auto flex-1 px-8 text-center pb-12">
			<h1 className="text-6xl font-bold">
				<span className="text-green-600">Energy</span> Audit
			</h1>
			<p className="mt-3 text-2xl">
			Fill out the form to receive an energy rating and advice
			</p>
			<Progress total={TOTAL_SECTIONS} current={section}/>

			{sections.map((eachSection, index) => {
				return (
				<Transition className='w-full' show={(!showResults && (section === (index + 1))) ? true: false }>
					<Section state={state} key={`section-${index}`} dispatch={dispatch} questions={eachSection} />
				</Transition>
				)
			})}

			<Results results={state} show={(showResults) ? true : false} />

			{!showResults ? (
			<div className='text-left w-full'>
				{(section > 1) ? <button onClick={lastSection} className='bg-gray-200 hover:bg-gray-300 rounded text-gray-900 px-6 py-3 mt-6 font-bold'>Previous</button> : ''}
				{(section < TOTAL_SECTIONS) ? <button onClick={nextSection} className='float-right bg-gray-200 hover:bg-gray-300 rounded text-gray-900 px-6 py-3 mt-6 font-bold'>Next</button> : <button onClick={submit} className='float-right bg-green-500 hover:green-400 text-white rounded font-bold px-6 py-3 mt-6'>Submit</button>}
			</div>
			) : '' }

		</main>
		</div>
	)
}
