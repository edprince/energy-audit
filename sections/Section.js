import { useEffect } from "react"
import { Transition } from "@headlessui/react"
import Question from "../components/Question"
import Range from "../components/Range"

export default function Section(props) {
	return (
		<>
			{props.questions.map(question => (
				<div className=''>
					<Question
						title={question.title}
						description={question.description}
						>
						{(question.type == "text") ? 
							<input onChange={(e) => props.dispatch({type: "set-response", questionId: question.questionId, response: e.target.value})} type='text' className='w-full rounded bg-gray-100 p-2 outline-none focus:outline-none' placeholder='Here is a placeholder'/>
						: '' }

						{(question.type == "range") ?
							<Range 
								options={question.options} 
								dispatch={props.dispatch}
								res={{type: "set-response", questionId: question.questionId}} /> 
							: '' }

					</Question>
				</div>
			))}
		</>
	)
}