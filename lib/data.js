const sections = [
	//Section One
	[
		{
			questionId: "q1",
			title: "Here is question number one",
			description: "And here is some explanation text to go with the question",
			dependent_on: [],
			type: "text"
		},
		{
			questionId: "q2",
			title: "Here is question number two",
			description: "And here is some explanation text to go with the question",
			dependent_on: [],
			type: "range",
			options: {
				0: "Don't know",
				1: "No",
				2: "Yes"
			}
		},
		{
			questionId: "q3",
			title: "This question will affect the lighting graph",
			description: "And here is some explanation text to go with the question",
			dependent_on: [],
			type: "range",
			options: {
				0: "Don't know",
				1: "Never",
				2: "Rarely",
				3: "Sometimes",
				4: "Often",
				5: "Regularly"
			}
		},
	],
	[
		{
			questionId: "q4",
			title: "Here is question number four",
			description: "And here is some explanation text to go with the question",
			dependent_on: [],
			type: "text"
		},
		{
			questionId: "q5",
			title: "This question will affect the heating graph",
			description: "And here is some explanation text to go with the question",
			dependent_on: [],
			type: "range",
			options: {
				0: "Don't know",
				1: "No",
				2: "Yes"
			}
		},
		{
			questionId: "q6",
			title: "This question will affect the appliances graph",
			description: "And here is some explanation text to go with the question",
			dependent_on: [],
			type: "range",
			options: {
				0: "Don't know",
				1: "Never",
				2: "Rarely",
				3: "Sometimes",
				4: "Often",
				5: "Regularly"
			}
		},
	]
];

export default sections;