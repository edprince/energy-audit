export default function getPercentage(a, b) {
	let result = Math.floor((a / b) * 100);
	if (isNaN(result)) return 0;
	return result;
}