Survey prototype app, written in Next.js. Offers multiple question types, with a results page.

Public demo: https://energy-survey.netlify.app/
